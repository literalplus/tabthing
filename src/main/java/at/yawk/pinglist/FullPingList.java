package at.yawk.pinglist;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yawkat
 */
public class FullPingList implements PingListLayout {
    public static final String ANY = "";

    private final List<PingListEntry> contents = new ArrayList<>();


    @Override
    public PingListEntry[] getContent() {
        PingListEntry[] content = new PingListEntry[60];
        for(int i = 0; i < contents.size(); i++) {
            if(i >= 60) {
                break;
            }
            content[i] = contents.get(i);
        }
        return content;
    }

    @Override
    public void add(PingListEntry entry, Object argument) {
        contents.add(entry);
    }

    @Override
    public void set(int index, PingListEntry entry, Object argument) {
        contents.set(index, entry);
    }

    @Override
    public void remove(PingListEntry entry) {
        contents.remove(entry);
    }

    @Override
    public void clear(Object section) {
        contents.clear();
    }
}
