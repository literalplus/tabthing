package at.yawk.pinglist;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Yawkat
 */
public class TestPlugin extends JavaPlugin implements Listener {
    public static boolean DEBUG;

    private AbstractPingListBuilder<Player> builder;
    private Chat chat;
    private FullPingList layout = new FullPingList();
    private Map<UUID, PingListEntry> playerEntries = new HashMap<>();
    private boolean addPrefix;

    @Override
    public void onEnable() {
        builder = new BukkitPingListBuilder(this);

        getConfig().options().copyHeader(true);
        getConfig().options().header("Tab Thing config. You'll be done in a second. Support in #lit on irc://irc.spi.gt.\nadd-prefix: Adds Vault prefix instead of just using display name (Essentials adds the prefix to that)");
        getConfig().options().copyDefaults(true);
        getConfig().addDefault("add-prefix", false);
        getConfig().addDefault("debug", false);
        this.addPrefix = getConfig().getBoolean("add-prefix");
        DEBUG = getConfig().getBoolean("debug");
        saveConfig();

        if (addPrefix && !setupChat()) {
            getLogger().severe("Get Vault pls");
        }

        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.MONITOR) //Catch all dem nicks
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        builder.init(player);
        builder.set(player, layout); //Send them the tablist without them first

        getServer().getScheduler().runTaskLater(this, new Runnable() {
            @Override
            public void run() {
                String tabDisplayName = getTabDisplayName(player);

                if (DEBUG) {
                    getLogger().info("Tabname of " + player.getName() + ": " + tabDisplayName + "; bukkit=" + player.getPlayerListName());
                }
                if (tabDisplayName.length() > PingConstants.MAXIMUM_MESSAGE_LENGTH) {
                    getLogger().warning("Sanitizing too long name: " + tabDisplayName);
                    tabDisplayName = tabDisplayName.substring(0, PingConstants.MAXIMUM_MESSAGE_LENGTH);
                }

                PingListEntry pingListEntry = new PingListEntry(tabDisplayName);
                playerEntries.put(player.getUniqueId(), pingListEntry);
                layout.add(pingListEntry, FullPingList.ANY); //Now add the player to the global layout - Essentials delays setting of display names

                setForAll();
            }
        }, 20L);
    }

    private String getTabDisplayName(Player player) {
        if (addPrefix) {
            return ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(player)) +
                    player.getDisplayName();
        } else {
            return player.getDisplayName(); //Essentials parses colors for us, yay
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent evt) {
        layout.remove(playerEntries.remove(evt.getPlayer().getUniqueId()));
        setForAll();
    }

    private void setForAll() {
        for (Player plr : getServer().getOnlinePlayers()) {
            builder.set(plr, layout);
        }
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        return (chat != null);
    }
}
