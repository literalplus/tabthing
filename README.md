# Tab Thing
'aka: what happens when you out time pressure on me and don't leave time to optimize and prettify stuffs. I initailly thought this project would be finished in 5 minutes, so please don't kill me right now. I know about its issues and if it really hurts you, please PR fixes. This project is on here for reference purposes and should not be used to learn from. Thank you!'

This plugin adds prefixes to Bukkit server tablists using [yawkat's TabAPI](https://github.com/yawkat/TabApi). The prefixes are fetched using Vault and can be up to 16 characters.
Idea by KevinEssence.
To run this, you need the following plugins (Google has them):

 * ProtocolLib
 * Vault

![Fancy tablist using this plugin (Screenshot courtesy of KevinEssence)](https://i.imgur.com/khwXEoe.png)

## Code note
The code was just forked from yawkat's repo quick'n'dirty, since this plugin is so small that um yeah (It only consists of one class).

## Building

````
mvn clean package
````
or get the binary from [my CI server](http://ci.nowak-at.net/job/public~tabthing/)